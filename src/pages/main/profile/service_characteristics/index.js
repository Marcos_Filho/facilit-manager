import React, { useEffect, useState } from 'react';
import { Container, Footer } from '../styles';
import { Input } from '../../../../components/ui/inputs'
import Header from '../../../../components/ui/header';
import { Picker } from '@react-native-picker/picker';
import { View } from '../../../../components/ui/inputs/styles';
import { getHeader } from '../../../../components/controllers/auth';
import api from '../../../../components/controllers/api';

const service_characteristics = (props) => {
  const [selectedDuration, setSelectedDuration] = useState();
  const [isLoading, setLoading] = useState(true);
  const serviceId = props.route.params?.id;
  const [name, setName] = useState(null)
  const [price, setPrice] = useState(null)
  const [description, setDescription] = useState(null)
  const [category, setCategory] = useState(null)
  const [time, setTime] = useState(null)

  useEffect(() => {
    if (serviceId) {
      api.get('services/' + serviceId, getHeader())
        .then(response => {
          setName(response.data.name)
          setPrice(response.data.price + "")
          setDescription(response.data.description)
          setCategory(response.data.category)
          setTime(response.data.time)

        })
        .catch(console.log)
        .finally(() => setLoading(false))
    }
  }, [])

  const updateService = async => {
    api.patch('services/' + serviceId, { "name": name, "price": parseFloat(price), "description": description }, getHeader())
      .then(response => props.navigation.navigate("Service"))
      .catch(err => console.log(err.request.response))
  }

  const createService = async => {
    api.post('services/', {
      "name": name,
      "price": parseFloat(price),
      "description": description,
      "category": "aquela",
      "time": "12:00",
      "shop": {
        "id": 1
      }
    }, getHeader())
      .then(response => props.navigation.navigate("Service"))
      .catch(err => console.log(err.request.response))
  }

  return (
    <Container>
      <Header title={"Editar serviço"} onPress={() => props.navigation.navigate("Service")} />
      <Input placeholder={"Nome do serviço"} value={name} marginTop={0} onChangeText={name => setName(name)} />
      <View marginTop={13}>
        <Picker
          style={{ flex: 1 }}
          selectedValue={selectedDuration}
          onValueChange={(itemValue, itemIndex) =>
            setSelectedDuration(itemValue)
          }>
          <Picker.Item label="5 minutos" value="5" />
          <Picker.Item label="10 minutos" value="10" />
          <Picker.Item label="15 minutos" value="15" />
          <Picker.Item label="20 minutos" value="20" />
          <Picker.Item label="25 minutos" value="25" />
          <Picker.Item label="30 minutos" value="30" />
          <Picker.Item label="40 minutos" value="40" />
          <Picker.Item label="50 minutos" value="50" />
          <Picker.Item label="60 minutos (1h00)" value="60" />
          <Picker.Item label="90 minutos (1h30)" value="90" />
          <Picker.Item label="2 Horas" value="120" />

        </Picker>
      </View>

      <Input placeholder={"Preço R$"} value={price} type={'numeric'} marginTop={13} onChangeText={price => setPrice(price)} />
      <Input placeholder={"Descrição"} value={description} marginTop={13} height={100} onChangeText={description => setDescription(description)} />

      <View marginTop={13}>
        <Picker
          style={{ flex: 1 }}
          selectedValue={selectedDuration}
          onValueChange={(itemValue, itemIndex) =>
            setSelectedDuration(itemValue)
          }>
          <Picker.Item label={"Cabelo"} value="Cabelo" />
          <Picker.Item label="Barba" value="Barba" />
          <Picker.Item label="Sobrancelha" value="Sobrancelha" />
        </Picker>
      </View>

      <Footer height={250} onPress={() => serviceId ? updateService() : createService()} />
    </Container>
  );
}

export default service_characteristics;