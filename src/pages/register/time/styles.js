import styled from 'styled-components/native';
import { mid_gray } from '../../../components/colors'
import { h } from '../../../components/dimens';

export const Line = styled.View`
  background-color: ${mid_gray};
  width: 100%;
  height: ${h(1)}px;
`;

export const TouchableOpacity = styled.TouchableOpacity`
  align-items: center; 
  flex-direction: row;
  height: ${h(36)}px;
`;