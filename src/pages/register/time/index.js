import React, { useState, useEffect } from 'react';
import { Container, Footer } from '../styles';
import Header from '../../../components/ui/header';
import { View } from 'react-native';
import Time_list from '../../../components/ui/lists/time_list';

const time = (props) => {

    const [monday, setMonday] = useState(null)
    const [tuesday, setTuesday] = useState(null)
    const [wednesday, setWednesday] = useState(null)
    const [thursday, setThursday] = useState(null)
    const [friday, setFriday] = useState(null)
    const [saturday, setSaturday] = useState(null)
    const [sunday, setSunday] = useState(null)

    useEffect(() => {
        if  (props.route.params)
            updateData(props.route.params);
    });

    const updateData = data => {
        switch (data.weekday) {
            case 0:
                setSunday(data.timeSelected)
                break;
            case 1:
                setMonday(data.timeSelected)
                break;
            case 2:
                setTuesday(data.timeSelected)
                break;
            case 3:
                setWednesday(data.timeSelected)
                break;
            case 4:
                setThursday(data.timeSelected)
                break;
            case 5:
                setFriday(data.timeSelected)
                break;
            case 6:
                setSaturday(data.timeSelected)
                break;
        }
    }

    return (
        <Container>
            <Header title={"Horário de funcionamento"}  onPress={() => props.navigation.goBack()}/>
            <View>
                <Time_list title={"Segunda-feira"} navigation={props.navigation} weekday={1}
                    timeSelected={monday}/>

                <Time_list title={"Terça-feira"} navigation={props.navigation} weekday={2}
                    timeSelected={tuesday}/>

                <Time_list title={"Quarta-feira"} navigation={props.navigation} weekday={3}
                    timeSelected={wednesday}/>

                <Time_list title={"Quinta-feira"} navigation={props.navigation} weekday={4}
                    timeSelected={thursday}/>

                <Time_list title={"Sexta-feira"} navigation={props.navigation} weekday={5}
                    timeSelected={friday}/>

                <Time_list title={"Sábado"} navigation={props.navigation} weekday={6}
                    timeSelected={saturday}/>

                <Time_list title={"Domingo"} navigation={props.navigation} weekday={0}
                    timeSelected={sunday}/>
            </View>
            <Footer onPress={() => props.navigation.navigate("Category")} />
        </Container>
    );
}

export default time;