import React, { useState } from 'react';
import { Container, Footer } from '../styles';
import Header from '../../../components/ui/header';
import { View } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import { CustomText } from '../../../components/ui/texts';
import { h } from '../../../components/dimens';
import WheelPicker from '../../../components/ui/wheelPicker';
import { mid_gray, primary_color } from '../../../components/colors';

const select_break = (props) => {
    const [startBreak, setStartBreak] = useState("00:00");
    const [finalBreak, setFinalBreak] = useState("00:00");

    let hours = [];
    let minutes = [];
    for (let h = 0; h < 24; h++) {
        let controler = h;
        if (h < 10)
            controler = "0" + h
        hours[h] = controler;
    }
    for (let h = 0; h < 6; h++) {
        let controler = h * 10;
        if (h < 1)
            controler = "0" + controler
        minutes[h] = controler;
    }

    const goBack = () => {
        if (validate()) {
            props.navigation.dispatch(
                CommonActions.navigate({
                    name: 'Select_time',
                    params: { startBreak: startBreak, finalBreak: finalBreak },
                })
            );
        }
    }

    const validate = () => {
        return true
    }

    const setTime = (time, hour, selectedTime, setSelectedTime) => {
        const dateSplit = selectedTime.split(":")
        let formtedData = "";
        if (hour)
            formtedData = time + ":" + dateSplit[1]
        else
            formtedData = dateSplit[0] + ":" + time

        setSelectedTime(formtedData)
    }

    return (
        <Container>
            <Header title={"Selecionar horário"} onPress={() => props.navigation.goBack()} />
            <CustomText fontSize={10} weight={"SemiBold"}  >#Aqui vem um scroll#</CustomText>
            <View style={{ flexDirection: 'row', alignSelf: "center" }}>
                <WheelPicker item={hours} onChange={(data) => setTime(data.label, true, startBreak, setStartBreak)} />
                <WheelPicker item={minutes} onChange={(data) => setTime(data.label, false, startBreak, setStartBreak)} />
                <View style={{ height: h(300), alignItems: 'center', justifyContent: "center" }}>
                    <CustomText fontSize={16} weight={"Medium"} color={primary_color} style={{ fontSize: 30, marginBottom: h(5) }}>:</CustomText>
                </View>
                <WheelPicker item={hours} onChange={(data) => setTime(data.label, true, finalBreak, setFinalBreak)} />
                <WheelPicker item={minutes} onChange={(data) => setTime(data.label, false, finalBreak, setFinalBreak)} />
            </View>
            <View style={{ backgroundColor: mid_gray(), height: h(1), width: `100%` }} />
            <Footer onPress={() => goBack()} />
        </Container>
    );
}

export default select_break;