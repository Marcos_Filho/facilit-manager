import React, { useState, useEffect } from 'react';
import { Container, Footer, TopView } from '../styles';
import Header from '../../../components/ui/header';
import { TouchableOpacity, View } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import { CustomText } from '../../../components/ui/texts';
import { h, w } from '../../../components/dimens';
import { Feather } from '@expo/vector-icons';
import { mid_gray, primary_color, secondary_color } from '../../../components/colors';
import WheelPicker from '../../../components/ui/wheelPicker';

const select_time = (props) => {
    const [openingTime, setOpeningTime] = useState("00:00");
    const [closingTime, setClosingTime] = useState("00:00");
    const [startBreak, setStartBreak] = useState(null);
    const [finalBreak, setFinalBreak] = useState(null);

    const weekday = props.route.params.weekday

    let hours = [];
    let minutes = [];
    for (let h = 0; h < 24; h++) {
        let controler = h;
        if (h < 10)
            controler = "0" + h
        hours[h] = controler;
    }
    for (let h = 0; h < 6; h++) {
        let controler = h * 10;
        if (h < 1)
            controler = "0" + controler
        minutes[h] = controler;
    }

    useEffect(() => {
        if  (props.route.params.startBreak) {
            updateData(props.route.params);
        }
    });

    const updateData = data => {
        setStartBreak(data.startBreak)
        setFinalBreak(data.finalBreak)
    }


    const goBack = () => {
        props.navigation.dispatch(
            CommonActions.navigate({
                name: 'Time',
                params: { weekday: weekday, timeSelected: openingTime + " - " + closingTime },
            })
        );
    }

    const setTime = (time, hour, selectedTime, setSelectedTime) => {
        const dateSplit = selectedTime.split(":")
        let formtedData = "";
        if (hour)
            formtedData = time + ":" + dateSplit[1]
        else
            formtedData = dateSplit[0] + ":" + time

        setSelectedTime(formtedData)
    }

    return (
        <Container>
            <Header title={"Selecionar horário"} onPress={() => props.navigation.goBack()} />
            <View style={{ flexDirection: 'row', alignSelf: "center" }}>
                <WheelPicker item={hours} onChange={(data) => setTime(data.label, true, openingTime, setOpeningTime)} />
                <WheelPicker item={minutes} onChange={(data) => setTime(data.label, false, openingTime, setOpeningTime)} />
                <View style={{ height: h(300), alignItems: 'center', justifyContent: "center" }}>
                    <CustomText fontSize={16} weight={"Medium"} color={primary_color} style={{ fontSize: 30, marginBottom: h(5) }}>:</CustomText>
                </View>
                <WheelPicker item={hours} onChange={(data) => setTime(data.label, true, closingTime, setClosingTime)} />
                <WheelPicker item={minutes} onChange={(data) => setTime(data.label, false, closingTime, setClosingTime)} />
            </View>
            <View style={{ backgroundColor: mid_gray(), height: h(1), width: `100%` }} />
            {startBreak != null ?
                <View>
                    <CustomText fontSize={16} weight={"Medium"} style={{marginTop: w(16)}}>Intervalo das {startBreak} as {finalBreak}</CustomText>
                    <TouchableOpacity style={{ width: h(200), alignItems: 'center', marginTop: w(4), flexDirection: 'row' }} onPress={() => props.navigation.navigate("Select_break")}>
                        <Feather name={"edit"} color={secondary_color()} size={h(16)} />
                        <CustomText fontSize={16} weight={"Medium"} color={secondary_color}>Alterar Intervalo</CustomText>
                    </TouchableOpacity>
                </View> 
                :
                <View style={{ flexDirection: 'row' }}>
                    <TouchableOpacity style={{ width: h(200), alignItems: 'center', marginTop: w(16), flexDirection: 'row' }} onPress={() => props.navigation.navigate("Select_break")}>
                        <Feather name={"plus"} color={secondary_color()} size={h(16)} />
                        <CustomText fontSize={16} weight={"Medium"} color={secondary_color}>Adicionar Intervalo</CustomText>
                    </TouchableOpacity>
                </View>

            }
            
            <Footer onPress={() => goBack()} />
        </Container>
    );
}

export default select_time;