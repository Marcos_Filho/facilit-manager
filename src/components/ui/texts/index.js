import React from 'react';
import { gray, primary_color, secondary_color, third_color, white, red } from '../../colors'
import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize'
import { Dimensions } from 'react-native'
import { useFonts } from 'expo-font';
import { Text } from 'react-native';
    
export const CustomText = (props) => {
    const [loaded] = useFonts({
        MontserratBold: require('../../../assets/fonts/Montserrat-Bold.ttf'),
        MontserratSemiBold: require('../../../assets/fonts/Montserrat-SemiBold.ttf'),
        MontserratMedium: require('../../../assets/fonts/Montserrat-Medium.ttf'),
        Montserrat: require('../../../assets/fonts/Montserrat.ttf'),
    });

    if (!loaded) {
        return null
    }

    const style = [
        { 
            color: props.color? props.color() :  primary_color(), 
            fontFamily: props.weight? "Montserrat"+props.weight : "Montserrat",
            fontSize: RFValue(props.fontSize, windowWidth),
        }, props.style || {}]
    
    const allProps = Object.assign({}, props, { style: style });

    return <Text {...allProps}>{props.children}</Text>

}

const windowWidth = Dimensions.get('window').width*1.6;