import React from 'react';
import WheelPickerExpo from 'react-native-wheel-picker-expo';
import { light_gray } from '../../colors';
import { h, w } from '../../dimens';

const wheelPicker = (props) => {
  return (
    <WheelPickerExpo
          height={h(300)}
          width={w(50)}
          backgroundColor={light_gray()}
          selectedStyle={{ borderWidth: h(1) }}
          initialSelectedIndex={0}
          items={props.item.map(name => ({ label: name }))}
          onChange={({ item }) => props.onChange(item)} />
  );
}

export default wheelPicker;