import React from 'react';
import { View } from 'react-native';
import { w } from '../../dimens';
import { Feather } from '@expo/vector-icons';
import { red, mid_gray } from '../../colors';
import { CustomText } from '../texts';
import { Container, Line, Touch } from './styles';

const Time_list = (props) => {

    return <View>
        {/*Lista de dias da semana com os horarios*/}
        <Container>
            <Touch onPress={() => props.navigation.navigate("Select_time", {
                weekday: props.weekday,
                timeSelected: props.timeSelected,
            })}>
                <CustomText fontSize={10}>{props.title}</CustomText>
                <CustomText color={!props.timeSelected && red} fontSize={10} style={{ position: `absolute`, left: w(140) }}>{props.timeSelected ? props.timeSelected : "Fechado"}</CustomText>
                <Feather name={"chevron-right"} color={mid_gray()} size={w(10)} style={{ position: `absolute`, right: w(4) }} />
            </Touch>
        </Container>
        <Line />
    </View>;
}

export default Time_list;
