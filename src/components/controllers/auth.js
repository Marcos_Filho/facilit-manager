export const getHeader = (config) => {
    if (!config) {
        config = {};
    }
    Object.assign(config, { Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NTA1NzY3NjIsImV4cCI6MTY1MDY2MzE2Mn0.Y4QyY8Wp7PctQRzeEKgusJDSButSIpN6NNo3T03tEf0`});
    return { headers: config };
};